package test.java.DronewarePack;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class AddWMSConfigTest {
	
	WebDriver driver;
	  
	@BeforeTest
	//Driver Initialization 
	public void TestSetup() {
		System.setProperty("webdriver.chrome.driver", "/home/konverge/Selenium/chromedriver");
		driver = new ChromeDriver();
			  
	}
	
	@Test (priority =1)
	//Login
	//@Parameters({"url","email","pwd"})
	public void BasicAuthLogin() throws InterruptedException {
	    
		Boolean LoginCheck;
		driver.get("https://emergys:emergys@2@ddev.apps.emergys.com");
	    driver.manage().window().maximize();
	    driver.manage().deleteAllCookies();
	    
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.id("email")).sendKeys("tejas.rupade@konverge.ai");
		driver.findElement(By.id("pwd")).sendKeys("Emergys@Admin");
		driver.findElement(By.id("login_btn")).click();
		
		Thread.sleep(6000);
		
		LoginCheck = driver.findElement(By.xpath("//h1[contains(text(),'Organization Information')]")).isDisplayed();

		if(LoginCheck){
			  System.out.println("Login Successful");
		  }
		  else
		  {
			  System.out.println("Login is not successful");
		  }
	 }
  
  @Test (priority =2)
  public void AddWMSConfig() throws InterruptedException {
	  
	  boolean wmsconfigAddCheck;
	  driver.findElement(By.xpath("//td[contains(text(),'AutoPTC')]")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("add_wms_config_btn")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("wms_name")).sendKeys("PTCWMSConfig");
	  
	  WebElement wmstype = driver.findElement(By.id("select_wms_type"));
	  Select wms = new Select(wmstype);
	  wms.selectByValue("SAP WMS");
	  
	  WebElement wmsIPIT = driver.findElement(By.id("select_wms_ip_interface_type"));
	  Select wmsip = new Select(wmsIPIT);
	  wmsip.selectByValue("FILE");
	  
	  WebElement wmsOPIT = driver.findElement(By.id("select_wms_op_interface_type"));
	  Select wmsop = new Select(wmsOPIT);
	  wmsop.selectByValue("FILE");
	  
	  driver.findElement(By.id("ip_field")).sendKeys("192.147.50.11");
	  driver.findElement(By.id("add_wms_config_submit_btn")).click();
	  Thread.sleep(2000);
	  
	  wmsconfigAddCheck = driver.findElement(By.xpath("//td[contains(text(),'PTCWMSConfig')]")).isDisplayed();
		 
	  if(wmsconfigAddCheck){
		  System.out.println("WMS Config record is added successfully");
	  }
	  else
	  {
		  System.out.println("WMS Config record is not added");
	  }
	  
}
 
  @AfterTest
  //Closing The Browser
  public void TestClose() {	  
	   driver.close();
  }

}

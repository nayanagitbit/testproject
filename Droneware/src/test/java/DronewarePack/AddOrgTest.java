package test.java.DronewarePack;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class AddOrgTest{
	
	WebDriver driver;
	  
	@BeforeTest
	//Driver Initialization 
	public void TestSetup() {
		System.setProperty("webdriver.chrome.driver", "/home/konverge/Selenium/chromedriver");
		driver = new ChromeDriver();
			  
	}
	
  @Test (priority =1)
  //Login
  //@Parameters({"url","email","pwd"})
  public void BasicAuthLogin() {
	  
	    Boolean LoginCheck;
	    driver.get("https://emergys:emergys@2@ddev.apps.emergys.com");
	    driver.manage().window().maximize();
	    driver.manage().deleteAllCookies();
	    
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.id("email")).sendKeys("tejas.rupade@konverge.ai");
		driver.findElement(By.id("pwd")).sendKeys("Emergys@Admin");
		driver.findElement(By.id("login_btn")).click();
		
		LoginCheck = driver.findElement(By.xpath("//h1[contains(text(),'Organization Information')]")).isDisplayed();

		if(LoginCheck){
			  System.out.println("Login Successful");
		  }
		  else
		  {
			  System.out.println("Login is not successful");
		  }
 }
  
  //Add Organization
  @Test (priority =2)
  public void AddOrg() throws AWTException, InterruptedException {
	  
	  boolean orgAddCheck;
	  String orgdate, orgnewdate;
	  driver.findElement(By.id("add_organization_btn")).click();
	  Date d = new Date(System.currentTimeMillis());
	  orgdate = d.toString();
      orgnewdate =  orgdate.replaceAll("\\s+","");
      System.out.println(orgnewdate);
	  
	  //Entering Organization Name
	  driver.findElement(By.id("organization_name")).sendKeys("AutoPTC"+orgnewdate);
	  Thread.sleep(2000);
	  //Uploading the logo
	  driver.findElement(By.id("org_logo_file")).click();
	  Thread.sleep(3000);
	  
	  StringSelection strSel = new StringSelection("/home/konverge/KonvergeAIData/DronewareFiles/PTCLogo.png");
	  Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		
	  //Create an object for robot class
	  Robot robot = new Robot();
	  //Ctrl+V 
	  robot.keyPress(KeyEvent.VK_CONTROL);
	  robot.keyPress(KeyEvent.VK_V);
	  robot.keyRelease(KeyEvent.VK_CONTROL);
	  robot.keyRelease(KeyEvent.VK_V);			
	  Thread.sleep(3000);
	  
	  robot.keyPress(KeyEvent.VK_ENTER);
	  robot.keyRelease(KeyEvent.VK_ENTER); 
	  
	  //Selecting License Key Start Date
	  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	  
	  //get current date time with Date()
	  Date date = new Date();
	  
	  // Now format the date
	  String startDate = dateFormat.format(date);
	  
	  // Print the Date
	  System.out.println("Current date and time is " +startDate); 
	  
	  driver.findElement(By.id("license_key_start_date")).sendKeys(startDate);
	  Thread.sleep(2000);
	  
	//Selecting License Key End Date
	  Calendar cal = Calendar.getInstance();
	  Date today = cal.getTime();
	  cal.add(Calendar.YEAR, 1); // to get previous year add -1
	  Date nextYear = cal.getTime();
	  String endDate = dateFormat.format(nextYear);
	  
	  System.out.println("Next Year date and time is " +endDate); 
	  
	  driver.findElement(By.id("license_key_end_date")).sendKeys(endDate);
	  Thread.sleep(3000);
	  
	  //Clicking Save Button
	  driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
	  Thread.sleep(3000);
	  
	  orgAddCheck = driver.findElement(By.xpath("//td[contains(text(),'AutoPTC')]")).isDisplayed();
		 
	  if(orgAddCheck){
		  System.out.println("Organization is added successfully");
	  }
	  else
	  {
		  System.out.println("Organization is not added.");
	  }
  }
      
  @AfterTest
  //Closing The Browser
  public void TestClose() {	  
	   driver.close();
  }
	  
}
